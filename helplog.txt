1.template
C++ Primer

2.access specifier
C++ Primer
http://www.jb51.net/article/54224.htm

3.vector
http://www.cplusplus.com/reference/vector/vector/
https://stackoverflow.com/questions/16868230/vector-in-namespace-std-does-not-name-a-type

4.QString
https://zhidao.baidu.com/question/388605732.html

5.pointer and reference
https://stackoverflow.com/questions/5541952/whats-the-difference-between-reference-and-pointer-return-types-in-c
http://www.cnblogs.com/CheeseZH/p/5163200.html

6.delete
http://blog.sina.com.cn/s/blog_9ce5a1b5010131nj.html

7.IO
https://stackoverflow.com/questions/15185801/cout-was-not-declared-in-this-scope

8.glm
http://glm.g-truc.net/0.9.7/api/a00213.html
https://www.opengl.org/discussion_boards/showthread.php/197957-Rotating-scaling-and-translating-2d-points-in-GLM
https://stackoverflow.com/questions/11515469/how-do-i-print-vector-values-of-type-glmvec3-that-have-been-passed-by-referenc

9.QT
http://www.kuqin.com/qtdocument/index.html
https://www.ibm.com/developerworks/cn/linux/guitoolkit/qt/signal-slot/

Unanswered Questions:
1. Why should I delete a passed-in pointer now that it was not created in the current scope? What if it is still in use outside?
Ans: As long as the pointer is deleted is it's on heap, it's OK u do not delete Polygon* in Node. Delete in where an object is created is a good style.
2. In Drawable now that you have virtual deconstructor, why you created a destroy() and called it in deconstructor, instead of directly write the code in deconstructor?
Ans: destroy() is specific for releasing resources concerning OpenGL.
3. Why QT create QString now that we have std::string?
Ans: QString fits in QT classes and functions better
4. Though deconstructor of base class is virtual, sub class is not achieving deconstructor by inherit, right?
Ans: It's not inherit. The function names are even unequal.
5. What's the meaning of writing code in virtual function now that it will certainly be overrided?
Ans: C++ virtual function is difference from Java abstract method. Virtual function can be executed without being overrided. C++ pure virtual function is similar to Java abstract method.
6. What if delete a pointer twice? Do I need to assign the pointer with nullptr after deleting?
Ans: Delete pointer twice will cause unexpected result. Assign a pointer nullptr after deletion is a good style.