#include "mainwindow.h"
#include <ui_mainwindow.h>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->mygl->setFocus();

    connect(ui->mygl, SIGNAL(rootCreated(QTreeWidgetItem*)), this, SLOT(on_receiving_rootNode(QTreeWidgetItem*)));
    connect(ui->addSquareButton, SIGNAL(clicked(bool)), this, SLOT(on_addSquareButton_Clicked(bool)));
    connect(this, SIGNAL(addSquareSignal(QTreeWidgetItem*)), ui->mygl, SLOT(addSquareSlot(QTreeWidgetItem*)));
    connect(ui->addTransNodeButton, SIGNAL(clicked(bool)), this, SLOT(on_addTransNodeButton_Clicked(bool)));
    connect(this, SIGNAL(addTransNodeSignal(QTreeWidgetItem*)), ui->mygl, SLOT(addTransNodeSlot(QTreeWidgetItem*)));
    connect(ui->addScaleNodeButton, SIGNAL(clicked(bool)), this, SLOT(on_addScaleNodeButton_Clicked(bool)));
    connect(this, SIGNAL(addScaleNodeSignal(QTreeWidgetItem*)), ui->mygl, SLOT(addScaleNodeSlot(QTreeWidgetItem*)));
    connect(ui->addRotateNodeButton, SIGNAL(clicked(bool)), this, SLOT(on_addRotateNodeButton_Clicked(bool)));
    connect(this, SIGNAL(addRotateNodeSignal(QTreeWidgetItem*)), ui->mygl, SLOT(addRotateNodeSlot(QTreeWidgetItem*)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_receiving_rootNode(QTreeWidgetItem* root)
{
    std::cout<<"root signal received"<<std::endl;
    ui->treeWidget->addTopLevelItem(root);
}

void MainWindow::on_addSquareButton_Clicked(bool isClicked)
{
    emit addSquareSignal(ui->treeWidget->currentItem());
}

void MainWindow::on_addTransNodeButton_Clicked(bool isClicked)
{
    emit addTransNodeSignal(ui->treeWidget->currentItem());
}

void MainWindow::on_addScaleNodeButton_Clicked(bool isClicked)
{
    emit addScaleNodeSignal(ui->treeWidget->currentItem());
}

void MainWindow::on_addRotateNodeButton_Clicked(bool isClicked)
{
    emit addRotateNodeSignal(ui->treeWidget->currentItem());
}

void MainWindow::on_actionQuit_triggered()
{
    QApplication::exit();
}
