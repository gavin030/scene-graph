#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidgetItem>
#include <iostream>
#include "node.h"


namespace Ui {
class MainWindow;
}


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void addSquareSignal(QTreeWidgetItem* node);
    void addTransNodeSignal(QTreeWidgetItem* node);
    void addScaleNodeSignal(QTreeWidgetItem* node);
    void addRotateNodeSignal(QTreeWidgetItem* node);

public slots:
    void on_receiving_rootNode(QTreeWidgetItem* root);
    void on_addSquareButton_Clicked(bool isClicked);
    void on_addTransNodeButton_Clicked(bool isClicked);
    void on_addScaleNodeButton_Clicked(bool isClicked);
    void on_addRotateNodeButton_Clicked(bool isClicked);

private slots:
    void on_actionQuit_triggered();

private:
    Ui::MainWindow *ui;
};


#endif // MAINWINDOW_H
