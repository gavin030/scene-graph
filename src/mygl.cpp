#include "mygl.h"
#include <la.h>

#include <iostream>
#include <QApplication>
#include <QKeyEvent>

MyGL::MyGL(QWidget *parent)
    : GLWidget277(parent),
      prog_flat(this),
      m_geomGrid(this), m_geomSquare(this, {glm::vec3(0.5f, 0.5f, 1.f),
                                            glm::vec3(-0.5f, 0.5f, 1.f),
                                            glm::vec3(-0.5f, -0.5f, 1.f),
                                            glm::vec3(0.5f, -0.5f, 1.f)}),
      m_showGrid(true), root(nullptr)
{
    setFocusPolicy(Qt::StrongFocus);
}

MyGL::~MyGL()
{
    makeCurrent();

    glDeleteVertexArrays(1, &vao);
    m_geomSquare.destroy();
    m_geomGrid.destroy();

    delete root;
}

void MyGL::initializeGL()
{
    // Create an OpenGL context using Qt's QOpenGLFunctions_3_2_Core class
    // If you were programming in a non-Qt context you might use GLEW (GL Extension Wrangler)instead
    initializeOpenGLFunctions();
    // Print out some information about the current OpenGL context
    debugContextVersion();

    // Set a few settings/modes in OpenGL rendering
//    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_POLYGON_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
    // Set the size with which points should be rendered
    glPointSize(5);
    // Set the color with which the screen is filled at the start of each render call.
    glClearColor(0.5, 0.5, 0.5, 1);

    printGLErrorLog();

    // Create a Vertex Attribute Object
    glGenVertexArrays(1, &vao);

    int n = 5;
    int& n2 = n;
    int n3 = n2;

    //Create the scene geometry
    m_geomGrid.create();
    m_geomSquare.create();

    // Create and set up the flat lighting shader
    prog_flat.create(":/glsl/flat.vert.glsl", ":/glsl/flat.frag.glsl");

    // We have to have a VAO bound in OpenGL 3.2 Core. But if we're not
    // using multiple VAOs, we can just bind one once.
    glBindVertexArray(vao);

    //initialize scene graph
    root = initializeSceneGraph();

    emit rootCreated(root);

    std::cout<<"root signal released"<<std::endl;
}

Node* MyGL::initializeSceneGraph()
{
    Node* root = new ScaleNode("Root", 2.f, 2.f);
    root->setPolygon(&m_geomSquare);
    root->setColor(glm::vec3(0, 1, 0));

    //neck and head
    Node* sNeckJoint = new ScaleNode("sNeckJoint", 0.5, 0.5);
    root->addChildNode(sNeckJoint);

    Node* tNeckJoint = new TranslateNode("tNeckJoint", 0, 1);
    sNeckJoint->addChildNode(tNeckJoint);

    Node* rNeckJoint = new RotateNode("rNeckJoint", 0);
    tNeckJoint->addChildNode(rNeckJoint);

    Node* sNeck = new ScaleNode("sNeck", 0.5, 1);
    rNeckJoint->addChildNode(sNeck);

    Node* tNeck = new TranslateNode("neck", 0, 0.5);
    tNeck->setPolygon(&m_geomSquare);
    tNeck->setColor(glm::vec3(1, 0, 0));
    sNeck->addChildNode(tNeck);

    Node* sHeadJoint = new ScaleNode("sHeadJoint", 2, 1);
    tNeck->addChildNode(sHeadJoint);

    Node* tHeadJoint = new TranslateNode("tHeadJoint", 0, 0.5);
    sHeadJoint->addChildNode(tHeadJoint);

    Node* rHeadJoint = new RotateNode("rHeadJoint", 30);
    tHeadJoint->addChildNode(rHeadJoint);

    Node* sHead = new ScaleNode("sHead", 1, 1);
    rHeadJoint->addChildNode(sHead);

    Node* tHead = new TranslateNode("head", 0, 0.5);
    tHead->setPolygon(&m_geomSquare);
    tHead->setColor(glm::vec3(0, 0, 1));
    sHead->addChildNode(tHead);

    //left upper arm and forearm
    Node* sLeftShoulder = new ScaleNode("sLeftShoulder", 0.5, 0.5);
    root->addChildNode(sLeftShoulder);

    Node* tLeftShoulder = new TranslateNode("tLeftShoulder", -1, 0.75);
    sLeftShoulder->addChildNode(tLeftShoulder);

    Node* rLeftShoulder = new RotateNode("rLeftShoulder", 0);
    tLeftShoulder->addChildNode(rLeftShoulder);

    Node* tLeftUpperArm = new TranslateNode("tLeftUpperArm", -0.5, 0);
    rLeftShoulder->addChildNode(tLeftUpperArm);

    Node* sLeftUpperArm = new ScaleNode("leftUpperArm", 1, 0.5);
    sLeftUpperArm->setPolygon(&m_geomSquare);
    sLeftUpperArm->setColor(glm::vec3(1, 0, 0));
    tLeftUpperArm->addChildNode(sLeftUpperArm);

    Node* sLeftElbow = new ScaleNode("sLeftElbow", 1, 2);
    sLeftUpperArm->addChildNode(sLeftElbow);

    Node* tLeftElbow = new TranslateNode("tLetElbow", -0.5, 0);
    sLeftElbow->addChildNode(tLeftElbow);

    Node* rLeftElbow = new RotateNode("rLeftElbow", -30);
    tLeftElbow->addChildNode(rLeftElbow);

    Node* tLeftForeArm = new TranslateNode("tLeftForeArm", -0.5, 0);
    rLeftElbow->addChildNode(tLeftForeArm);

    Node* sLeftForeArm = new ScaleNode("leftForeArm", 1, 0.5);
    sLeftForeArm->setPolygon(&m_geomSquare);
    sLeftForeArm->setColor(glm::vec3(0, 0, 1));
    tLeftForeArm->addChildNode(sLeftForeArm);

    //right upper arm and forearm
    Node* sRightShoulder = new ScaleNode("sRightShoulder", 0.5, 0.5);
    root->addChildNode(sRightShoulder);

    Node* tRightShoulder = new TranslateNode("tRightShoulder", 1, 0.75);
    sRightShoulder->addChildNode(tRightShoulder);

    Node* rRightShoulder = new RotateNode("rRightShoulder", 0);
    tRightShoulder->addChildNode(rRightShoulder);

    Node* tRightUpperArm = new TranslateNode("tRightUpperArm", 0.5, 0);
    rRightShoulder->addChildNode(tRightUpperArm);

    Node* sRightUpperArm = new ScaleNode("rightUpperArm", 1, 0.5);
    sRightUpperArm->setPolygon(&m_geomSquare);
    sRightUpperArm->setColor(glm::vec3(1, 0, 0));
    tRightUpperArm->addChildNode(sRightUpperArm);

    Node* sRightElbow = new ScaleNode("sRightElbow", 1, 2);
    sRightUpperArm->addChildNode(sRightElbow);

    Node* tRightElbow = new TranslateNode("tRightElbow", 0.5, 0);
    sRightElbow->addChildNode(tRightElbow);

    Node* rRightElbow = new RotateNode("rRightElbow", 30);
    tRightElbow->addChildNode(rRightElbow);

    Node* tRightForeArm = new TranslateNode("tRightForeArm", 0.5, 0);
    rRightElbow->addChildNode(tRightForeArm);

    Node* sRightForeArm = new ScaleNode("rightForeArm", 1, 0.5);
    sRightForeArm->setPolygon(&m_geomSquare);
    sRightForeArm->setColor(glm::vec3(0, 0, 1));
    tRightForeArm->addChildNode(sRightForeArm);

    //left upper leg and lower leg
    Node* sLeftUpperLegJoint = new ScaleNode("sLeftUpperLegJoint", 0.5, 0.5);
    root->addChildNode(sLeftUpperLegJoint);

    Node* tLeftUpperLegJoint = new TranslateNode("tLeftUpperLegJoint", -0.5, -1);
    sLeftUpperLegJoint->addChildNode(tLeftUpperLegJoint);

    Node* rLeftUpperLegJoint = new RotateNode("rLeftUpperLegJoint", 0);
    tLeftUpperLegJoint->addChildNode(rLeftUpperLegJoint);

    Node* tLeftUpperLeg = new TranslateNode("tLeftUpperLeg", 0, -0.5);
    rLeftUpperLegJoint->addChildNode(tLeftUpperLeg);

    Node* sLeftUpperLeg = new ScaleNode("leftUpperLeg", 0.5, 1);
    sLeftUpperLeg->setPolygon(&m_geomSquare);
    sLeftUpperLeg->setColor(glm::vec3(1, 0, 0));
    tLeftUpperLeg->addChildNode(sLeftUpperLeg);

    Node* sLeftLowerLegJoint = new ScaleNode("sLeftLowerLegJoint", 2, 1);
    sLeftUpperLeg->addChildNode(sLeftLowerLegJoint);

    Node* tLeftLowerLegJoint = new TranslateNode("tLeftLowerLegJoint", 0, -0.5);
    sLeftLowerLegJoint->addChildNode(tLeftLowerLegJoint);

    Node* rLeftLowerLegJoint = new RotateNode("rLeftLowerLegJoint", 0);
    tLeftLowerLegJoint->addChildNode(rLeftLowerLegJoint);

    Node* tLeftLowerLeg = new TranslateNode("tLeftLowerLeg", 0, -0.5);
    rLeftLowerLegJoint->addChildNode(tLeftLowerLeg);

    Node* sLeftLowerLeg = new ScaleNode("leftLowerLeg", 0.5, 1);
    sLeftLowerLeg->setPolygon(&m_geomSquare);
    sLeftLowerLeg->setColor(glm::vec3(0, 0, 1));
    tLeftLowerLeg->addChildNode(sLeftLowerLeg);

    //right upper leg and lower leg
    Node* sRightUpperLegJoint = new ScaleNode("sRightUpperLegJoint", 0.5, 0.5);
    root->addChildNode(sRightUpperLegJoint);

    Node* tRightUpperLegJoint = new TranslateNode("tRightUpperLegJoint", 0.5, -1);
    sRightUpperLegJoint->addChildNode(tRightUpperLegJoint);

    Node* rRightUpperLegJoint = new RotateNode("rRightUpperLegJoint", 0);
    tRightUpperLegJoint->addChildNode(rRightUpperLegJoint);

    Node* tRightUpperLeg = new TranslateNode("tRightUpperLeg", 0, -0.5);
    rRightUpperLegJoint->addChildNode(tRightUpperLeg);

    Node* sRightUpperLeg = new ScaleNode("rightUpperLeg", 0.5, 1);
    sRightUpperLeg->setPolygon(&m_geomSquare);
    sRightUpperLeg->setColor(glm::vec3(1, 0, 0));
    tRightUpperLeg->addChildNode(sRightUpperLeg);

    Node* sRightLowerLegJoint = new ScaleNode("sRightLowerLegJoint", 2, 1);
    sRightUpperLeg->addChildNode(sRightLowerLegJoint);

    Node* tRightLowerLegJoint = new TranslateNode("tRightLowerLegJoint", 0, -0.5);
    sRightLowerLegJoint->addChildNode(tRightLowerLegJoint);

    Node* rRightLowerLegJoint = new RotateNode("rRightLowerLegJoint", 0);
    tRightLowerLegJoint->addChildNode(rRightLowerLegJoint);

    Node* tRightLowerLeg = new TranslateNode("tRightLowerLeg", 0, -0.5);
    rRightLowerLegJoint->addChildNode(tRightLowerLeg);

    Node* sRightLowerLeg = new ScaleNode("rightLowerLeg", 0.5, 1);
    sRightLowerLeg->setPolygon(&m_geomSquare);
    sRightLowerLeg->setColor(glm::vec3(0, 0, 1));
    tRightLowerLeg->addChildNode(sRightLowerLeg);

    return root;
}

void MyGL::resizeGL(int w, int h)
{
    glm::mat3 viewMat = glm::scale(glm::mat3(), glm::vec2(0.2, 0.2)); // Screen is -5 to 5

    // Upload the view matrix to our shader (i.e. onto the graphics card)
    prog_flat.setViewMatrix(viewMat);

    printGLErrorLog();
}

//This function is called by Qt any time your GL window is supposed to update
//For example, when the function updateGL is called, paintGL is called implicitly.
void MyGL::paintGL()
{
    // Clear the screen so that we only see newly drawn images
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if(m_showGrid)
    {
        prog_flat.setModelMatrix(glm::mat3());
        prog_flat.draw(*this, m_geomGrid);
    }

    //VVV CLEAR THIS CODE WHEN YOU IMPLEMENT SCENE GRAPH TRAVERSAL VVV///////////////////

    // Shapes will be drawn on top of one another, so the last object
    // drawn will appear in front of everything else

//    prog_flat.setModelMatrix(glm::mat3());
//    m_geomSquare.setColor(glm::vec3(0,1,0));
//    prog_flat.draw(*this, m_geomSquare);

//    m_geomSquare.setColor(glm::vec3(1,0,0));
//    prog_flat.setModelMatrix(glm::translate(glm::mat3(), glm::vec2(-1.f, 0.f)) * glm::rotate(glm::mat3(), glm::radians(-30.f)));
//    prog_flat.draw(*this, m_geomSquare);

//    m_geomSquare.setColor(glm::vec3(0,0,1));
//    prog_flat.setModelMatrix(glm::translate(glm::mat3(), glm::vec2(1.f, 0.f)) * glm::rotate(glm::mat3(), glm::radians(30.f)));
//    prog_flat.draw(*this, m_geomSquare);

    traverseSceneGraph(root, glm::mat3());

    //^^^ CLEAR THIS CODE WHEN YOU IMPLEMENT SCENE GRAPH TRAVERSAL ^^^/////////////////

    // Here is a good spot to call your scene graph traversal function.
    // Any time you want to draw an instance of geometry, call
    // prog_flat.draw(*this, yourNonPointerGeometry);
}

void MyGL::traverseSceneGraph(Node *node, glm::mat3 T)
{
    T = T * node->getTransformation();

    for(auto child : node->getChildren()){
        traverseSceneGraph(child, T);
    }

    if(node->getPolygon() != nullptr){
        prog_flat.setModelMatrix(T);
        node->getPolygon()->setColor(node->getColor());
        prog_flat.draw(*this, *(node->getPolygon()));
    }
}

void MyGL::keyPressEvent(QKeyEvent *e)
{
    // http://doc.qt.io/qt-5/qt.html#Key-enum
    switch(e->key())
    {
    case(Qt::Key_Escape):
        QApplication::quit();
        break;

    case(Qt::Key_G):
        m_showGrid = !m_showGrid;
        break;
    }
}

void MyGL::addSquareSlot(QTreeWidgetItem *node)
{
    ((Node*)node)->setPolygon(&m_geomSquare);
}

void MyGL::addTransNodeSlot(QTreeWidgetItem *node)
{
    ((Node*)node)->addChildNode(new TranslateNode(((Node*)node)->getName() + "Child" + QString(((Node*)node)->getChildren().size())));
}

void MyGL::addScaleNodeSlot(QTreeWidgetItem *node)
{
    ((Node*)node)->addChildNode(new ScaleNode(((Node*)node)->getName() + "Child" + QString(((Node*)node)->getChildren().size())));
}

void MyGL::addRotateNodeSlot(QTreeWidgetItem *node)
{
    ((Node*)node)->addChildNode(new RotateNode(((Node*)node)->getName() + "Child" + QString(((Node*)node)->getChildren().size())));
}
