#include "node.h"

Node::Node(QString name)
    : name(name), children({}), polygon(nullptr), color(glm::vec3(0, 0, 0))
{
    this->setText(0, this->name);
//    QTreeWidgetItem::setText(1, this->name);
}

Node::~Node()
{
    delete polygon;
    for(unsigned int i = 0; i < children.size(); i++){
        delete children[i];
    }
}

glm::mat3 Node::getTransformation()
{}

Node* Node::addChildNode(Node* child)
{
    children.push_back(child);
    this->addChild(child);

    return child;
}

std::vector<Node*> Node::getChildren()
{
    return this->children;
}

QString Node::getName()
{
    return this->name;
}

glm::vec3 Node::getColor()
{
    return this->color;
}

void Node::setColor(glm::vec3 color)
{
    this->color = color;
}

Polygon2D* Node::getPolygon()
{
    return this->polygon;
}

void Node::setPolygon(Polygon2D *polygon)
{
    this->polygon = polygon;
}

TranslateNode::TranslateNode(QString name)
    : Node(name), tx(0), ty(0)
{}

TranslateNode::TranslateNode(QString name, float tx, float ty)
    : Node(name), tx(tx), ty(ty)
{}

TranslateNode::~TranslateNode()
{}

glm::mat3 TranslateNode::getTransformation()
{
    return glm::translate(glm::mat3(), glm::vec2(tx, ty));
}

float TranslateNode::getTx()
{
    return this->tx;
}

float TranslateNode::getTy()
{
    return this->ty;
}

void TranslateNode::setTx(float tx)
{
    this->tx = tx;
}

void TranslateNode::setTy(float ty)
{
    this->ty = ty;
}

RotateNode::RotateNode(QString name)
    : Node(name), rDegree(0)
{}

RotateNode::RotateNode(QString name, float rDegree)
    : Node(name), rDegree(rDegree)
{}

RotateNode::~RotateNode()
{}

glm::mat3 RotateNode::getTransformation()
{
    return glm::rotate(glm::mat3(), rDegree * PI / 180);
}

float RotateNode::getRDegree()
{
    return this->rDegree;
}

void RotateNode::setRDegree(float rDegree)
{
    this->rDegree = rDegree;
}

ScaleNode::ScaleNode(QString name)
    : Node(name), sx(1), sy(1)
{}

ScaleNode::ScaleNode(QString name, float sx, float sy)
    : Node(name), sx(sx), sy(sy)
{}

ScaleNode::~ScaleNode()
{}

glm::mat3 ScaleNode::getTransformation()
{
    return glm::scale(glm::mat3(), glm::vec2(sx, sy));
}

float ScaleNode::getSx()
{
    return this->sx;
}

float ScaleNode::getSy()
{
    return this->sy;
}

void ScaleNode::setSx(float sx)
{
    this->sx = sx;
}

void ScaleNode::setSy(float sy)
{
    this->sy = sy;
}

