#ifndef NODE_H
#define NODE_H

#include "la.h"
#include "utils.h"
#include "scene/polygon.h"
#include <vector>
#include <QString>
#include <QTreeWidgetItem>

class Node : public QTreeWidgetItem
{
//    Q_OBJECT

public:
    Node(QString name);
    virtual ~Node();

    virtual glm::mat3 getTransformation();
    Node* addChildNode(Node* child);
    std::vector<Node*> getChildren();
    QString getName();
    glm::vec3 getColor();
    void setColor(glm::vec3 color);
    Polygon2D* getPolygon();
    void setPolygon(Polygon2D* polygon);


protected:
    QString name;
    std::vector<Node*> children;
    Polygon2D* polygon;
    glm::vec3 color;
};

class TranslateNode : public Node
{
public:
    TranslateNode(QString name);
    TranslateNode(QString name, float tx, float ty);
    ~TranslateNode();
    glm::mat3 getTransformation() override;
    float getTx();
    float getTy();
    void setTx(float tx);
    void setTy(float ty);

protected:
    float tx, ty;
};

class RotateNode : public Node
{
public:
    RotateNode(QString name);
    RotateNode(QString name, float rDegree);
    ~RotateNode();
    glm::mat3 getTransformation() override;
    float getRDegree();
    void setRDegree(float rDegree);

protected:
    float rDegree;
};

class ScaleNode : public Node
{
public:
    ScaleNode(QString name);
    ScaleNode(QString name, float sx, float sy);
    ~ScaleNode();
    glm::mat3 getTransformation() override;
    float getSx();
    float getSy();
    void setSx(float sx);
    void setSy(float sy);

protected:
    float sx, sy;
};

#endif // NODE_H
